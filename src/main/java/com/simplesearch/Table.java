package com.simplesearch;

import lombok.Data;

@Data
public class Table {
    long userId;
    String email;
    String language;
    String programmingLanguage;
}
