package com.simplesearch.programminglanguage;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProgrammingLanguageRepository extends JpaRepository<ProgrammingLanguageEntity,Long> {
    List<ProgrammingLanguageEntity> findByNameLike(String name);
    ProgrammingLanguageEntity findByName(String name);
}
