package com.simplesearch.unittest;

import com.simplesearch.developer.DeveloperEntity;
import com.simplesearch.developer.DeveloperRepository;
import com.simplesearch.programminglanguage.ProgrammingLanguageEntity;
import com.simplesearch.programminglanguage.ProgrammingLanguageRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

//import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private DeveloperRepository developerRepository;

    @Autowired
    private ProgrammingLanguageRepository programmingLanguageRepository;

    @Test
    public void whenFindByEmail_thenReturnDevelopers() {
        DeveloperEntity sample = new DeveloperEntity("bony@a.com");

        entityManager.persistAndFlush(sample);
        DeveloperEntity found = developerRepository.findByEmail(sample.getEmail());
       assertThat(found.getEmail())
                .isEqualTo(sample.getEmail());
    }

    @Test
    public void whenFindByEmail_thenReturnPL() {
        ProgrammingLanguageEntity sample = new ProgrammingLanguageEntity("scala");

        entityManager.persistAndFlush(sample);
        ProgrammingLanguageEntity found = programmingLanguageRepository.findByName(sample.getName());
        assertThat(found.getName())
                .isEqualTo(sample.getName());
    }

}
