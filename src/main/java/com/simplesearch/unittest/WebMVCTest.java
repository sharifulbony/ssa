package com.simplesearch.unittest;

import com.simplesearch.developer.DeveloperEntity;
import com.simplesearch.developer.DeveloperRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(RestController.class)
public class WebMVCTest {


    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeveloperRepository developerRepository;

    @Test
    public void givenEmployees_whenGetEmployees_thenReturnJsonArray()
            throws Exception {

        DeveloperEntity developerEntity=new DeveloperEntity("natasha_gal_0407@hotmail.com");

        ArrayList<DeveloperEntity> allDevs= (ArrayList<DeveloperEntity>) developerRepository.findByEmailLike(developerEntity.getEmail());

//        given(service.getAllEmployees()).willReturn(allEmployees);

        mvc.perform(get("/api/alldev")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect((ResultMatcher) jsonPath("$",allDevs));
    }


}
